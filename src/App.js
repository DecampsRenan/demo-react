import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Switch,
} from 'react-router-dom'

import { Home } from './Home';
import DemoComponent1 from './demo-1/DemoComponent1';
import DemoComponent2 from './demo-2/DemoComponent2';
import DemoComponent3 from './demo-3/DemoComponent3';
import DemoComponent4 from './demo-4/DemoComponent4';
import { NotFound } from './NotFound';

const BasicExample = () => (
  <Router>
    <div className="container">
      <nav className="mt-3">
        <ul className="nav nav-pills">
          <li className="nav-item">
            <NavLink className="nav-link" exact to="/">Home</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/demo-1">Basic Component</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/demo-2">Simple counter</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/demo-3">Advanced counter</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/demo-4">HTTP Request example</NavLink>
          </li>
        </ul>
      </nav>

      <hr/>

      <div className="container">
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/demo-1" component={DemoComponent1} />
          <Route path="/demo-2" component={DemoComponent2} />
          <Route path="/demo-3" render={(props) => <DemoComponent3 counter="-10" timeout="300" {...props} />} />
          <Route path="/demo-4" component={DemoComponent4} />
          <Route component={NotFound} />
        </Switch>
      </div>
    </div>
  </Router>
)
export default BasicExample;