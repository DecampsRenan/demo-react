import React, { Fragment } from 'react';

export const NotFound = () => (
    <Fragment>
        <h1>Nice try !</h1>
        <small>But there is nothing here...</small>
    </Fragment>
);
