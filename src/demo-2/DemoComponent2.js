import React, { Component, Fragment } from 'react';

class DemoComponent2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
        };
        this.intervalRef = null;
    }

    componentDidMount() {
        this.intervalRef = setInterval(() => {
            const { counter } = this.state;
            this.setState({ counter: counter + 1 });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.intervalRef);
    }

    render() {
        const { counter } = this.state;
        return (
            <Fragment>
                <h1>Simple Counter</h1>
                <p>{counter}</p>
            </Fragment>
        )
    }
}

export default DemoComponent2;