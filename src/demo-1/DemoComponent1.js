import React from 'react';

const name = 'World';

const DemoComponent1 = () => (
    <div>
        <h1>Hello {name}</h1>
    </div>
);

export default DemoComponent1;