import React, { Fragment, Component } from 'react';

// DUMB COMPONENT
const ViewOnly = ({ counter }) => <p>{counter}</p>;

// SMART COMPONENT
class DemoComponent3 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            counter: props.counter,
        };

        this.intervalRef = null;
    }

    componentDidMount() {
        const { timeout } = this.props;
        this.intervalRef = setInterval(() => {
            const { counter } = this.state;
            this.setState({ counter: parseInt(counter, 10) + 1 });
        }, timeout);
    }

    componentWillUnmount() {
        clearInterval(this.intervalRef);
    }

    render() {
        const { counter } = this.state;
        return (
            <Fragment>
                <h1>Advanced Counter</h1>
                <ViewOnly counter={counter} />
            </Fragment>
        );
    }
}


export default DemoComponent3;