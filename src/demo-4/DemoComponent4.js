import React, { Component, Fragment } from 'react';
import { CharactersList } from './CharactersList';
import axios from 'axios';

const BASE_URL_API = 'https://rickandmortyapi.com/api';

export default class DemoComponent4 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            characters: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        axios.get(`${BASE_URL_API}/character`)
            .then(({ data }) => this.setState({
                characters: data.results,
                isLoading: false
            }))
            .catch((error) => {
                console.warn('Oups...', error)
                this.setState({ isLoading: false });
            });
    }

    render() {
        const { isLoading, characters } = this.state;
        return (
            <Fragment>
                <h1>Making HTTP Requests</h1>
                {isLoading ? 
                    <p>Loading characters...</p>
                    :
                    <CharactersList characters={characters} />
                }
            </Fragment>
        );
    }
}