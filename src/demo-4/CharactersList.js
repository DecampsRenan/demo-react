import React, { Fragment } from 'react';

const renderCharacterData = ({ id, image, name, status, species, gender }) => (
    <tr key={id}>
        <td>
            <img
                width="80"
                className="rounded"
                src={image}
                alt={name}
            />
        </td>
        <td>{name}</td>
        <td>{status}</td>
        <td>{species}</td>
        <td>{gender}</td>
    </tr>
);

export const CharactersList = ({ characters }) => (
    <Fragment>
        {characters.length <= 0 ?
            <p>No Characters.</p>
            :
            <table className="table table-hover">
                <thead>
                    <tr>
                        <td>Picture</td>
                        <td>Name</td>
                        <td>Status</td>
                        <td>Species</td>
                        <td>Gender</td>
                    </tr>
                </thead>
                <tbody>
                    {characters.map(renderCharacterData)}
                </tbody>    
            </table>
        }
    </Fragment>
);